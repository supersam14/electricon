import machine
# start of file
#
class AnalogPin:
    def __init__(self, _pin):

        self.pin = machine.Pin(_pin, machine.Pin.IN)
        self.adc = machine.ADC(self.pin)
        self.value = 0
        self.value = self.adc.read()

    def update(self):
        print('updating adc1...',end=' ')
        reading = self.adc.read()
        print("reading: ",end=' ')
        print(reading,end=' ')

        if reading > 4090:
            self.value = 0
        elif reading > 3500:
            self.value = 1
        elif reading > 3000:
            self.value = 2
        elif reading > 2600:
            self.value = 3
        elif reading > 2290:
            self.value = 4
        elif reading > 2290:            # bug here!
            self.value = 5
        elif reading > 1900:
            self.value = 6
        elif reading > 1700:
            self.value = 7
        elif reading > 1400:
            self.value = 8
        elif reading > 1100:
            self.value = 9
        elif reading > 900:
            self.value = 10




        else:
            print("adc: no value found")

        print(self.value)

    def read(self):
        return self.value

# end of file
