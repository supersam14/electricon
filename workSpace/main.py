# Combined File: ################### to be uploaded to esp32 by upload script using ampy##########
# config file

# DEBOUNCE_TIME = 20 # button debounce time in ms.

# end of config file
# end of file. ############################################################################ all global imports here
import machine
from time import sleep
from time import ticks_us
from time import ticks_ms
from time import ticks_diff

# end of file. ###########################################################################import machine
# start of file
#
class AnalogPin:
    def __init__(self, _pin):

        self.pin = machine.Pin(_pin, machine.Pin.IN)
        self.adc = machine.ADC(self.pin)
        self.value = 0
        self.value = self.adc.read()

    def update(self):
        print('updating adc1...',end=' ')
        reading = self.adc.read()
        print("reading: ",end=' ')
        print(reading,end=' ')

        if reading > 4090:
            self.value = 0
        elif reading > 3500:
            self.value = 1
        elif reading > 3000:
            self.value = 2
        elif reading > 2600:
            self.value = 3
        elif reading > 2290:
            self.value = 4
        elif reading > 2290:            # bug here!
            self.value = 5
        elif reading > 1900:
            self.value = 6
        elif reading > 1600:
            self.value = 7
        elif reading > 1300:
            self.value = 8
        elif reading > 1100:
            self.value = 9
        elif reading > 850:
            self.value = 10




        else:
            print("adc: no value found")

        print(self.value)

    def read(self):
        return self.value

# end of file
# end of file. ############################################################################# start of file mymodule.net

def hsv_to_rgb(h, s, v):
    """
    Convert HSV to RGB (based on colorsys.py).

        Args:
            h (float): Hue 0 to 1.
            s (float): Saturation 0 to 1.
            v (float): Value 0 to 1 (Brightness).
    """
    if s == 0.0:
        return v, v, v
    i = int(h * 6.0)
    f = (h * 6.0) - i
    p = v * (1.0 - s)
    q = v * (1.0 - s * f)
    t = v * (1.0 - s * (1.0 - f))
    i = i % 6

    v = int(v * 255)
    t = int(t * 255)
    p = int(p * 255)
    q = int(q * 255)

    if i == 0:
        return v, t, p
    if i == 1:
        return q, v, p
    if i == 2:
        return p, v, t
    if i == 3:
        return p, q, v
    if i == 4:
        return t, p, v
    if i == 5:
        return v, p, q

## end of file# end of file. ###########################################################################
# start of file


class PCF8574:
    def __init__(self, addr, i2c):
        self.pinMode = [0, 0, 0, 0, 0, 0, 0, 0]
        self.states = [0, 0, 0, 0, 0, 0, 0, 0]
        self.buf = 0
        self.last_updated = ticks_ms()
        self.i2c = i2c

        # self.i2c.init()
        self.addr = addr

        # set outputs?
        # self.i2c.write(0b11111111)
        # self.i2c.write(0b00000000)

        # deactivated address-validator
        # adresses = self.i2c.scan()
        # if not addr in adresses:
        #     print('desired adress not available')
        # else:
        #     self.addr = addr

    def update(self):
        self.buf = self.i2c.readfrom(self.addr, 64)
        # self.buf = '23463453'
        self.decode(self.buf)

    def decode(self, data_in):

        self.buf = str(data_in)
        self.buf = self.buf[4:6]
        self.buf = self.hex_to_dec(self.buf)
        self.buf = int(self.buf)
        # self.bata_buffer = '{:08b}'.format(self.data_buffer)
        # self.bata_buffer = format(data_buffer, '#010b') ## convert to bit format
        # print('read data:')
        # print(self.buf)

        if self.buf > 127:
            self.states[7] = 1
            self.buf -= 128
        else:
            self.states[7] = 0

        if self.buf > 63:
            self.states[6] = 1
            self.buf -= 64
        else:
            self.states[6] = 0

        if self.buf > 31:
            self.states[5] = 1
            self.buf -= 32
        else:
            self.states[5] = 0

        if self.buf > 15:
            self.states[4] = 1
            self.buf -= 16
        else:
            self.states[4] = 0

        if self.buf > 7:
            self.states[3] = 1
            self.buf -= 8
        else:
            self.states[3] = 0

        if self.buf > 3:
            self.states[2] = 1
            self.buf -= 4
        else:
            self.states[2] = 0

        if self.buf > 1:
            self.states[1] = 1
            self.buf -= 2
        else:
            self.states[1] = 0

        if self.buf == 1:
            self.states[0] = 1
        else:
            self.states[0] = 0

    def hex_to_dec(self, input_string):

        ans = 0
        string = input_string

        if len(string) == 2:

            if string[1] == '0':
                ans += 0
            elif string[1] == '1':
                ans += 1
            elif string[1] == '2':
                ans += 2
            elif string[1] == '3':
                ans += 3
            elif string[1] == '4':
                ans += 4
            elif string[1] == '5':
                ans += 5
            elif string[1] == '6':
                ans += 6
            elif string[1] == '7':
                ans += 7
            elif string[1] == '8':
                ans += 8
            elif string[1] == '9':
                ans += 9
            elif string[1] == 'a':
                ans += 10
            elif string[1] == 'b':
                ans += 11
            elif string[1] == 'c':
                ans += 12
            elif string[1] == 'd':
                ans += 13
            elif string[1] == 'e':
                ans += 14
            elif string[1] == 'f':
                ans += 15

            if string[0] == '0':
                ans += 16 * 0
            elif string[0] == '1':
                ans += 16 * 1
            elif string[0] == '2':
                ans += 16 * 2
            elif string[0] == '3':
                ans += 16 * 3
            elif string[0] == '4':
                ans += 16 * 4
            elif string[0] == '5':
                ans += 16 * 5
            elif string[0] == '6':
                ans += 16 * 6
            elif string[0] == '7':
                ans += 16 * 7
            elif string[0] == '8':
                ans += 16 * 8
            elif string[0] == '9':
                ans += 16 * 9
            elif string[0] == 'a':
                ans += 16 * 10
            elif string[0] == 'b':
                ans += 16 * 11
            elif string[0] == 'c':
                ans += 16 * 12
            elif string[0] == 'd':
                ans += 16 * 13
            elif string[0] == 'e':
                ans += 16 * 14
            elif string[0] == 'f':
                ans += 16 * 15
            # print(reading)
            return ans
        else:
            print('invalid string!')


class PCF8574_Array:
    def __init__(self, i2c, addr_list):
        self.i2c = i2c

        self.boards = []

        for addr in addr_list:
            chip = PCF8574(addr, self.i2c)
            self.boards.append(chip)

        print("initialisation complete. found and connected to",end=' ')
        print(len(self.boards),end=' ')
        print("boards.")

        self.num_boards = len(self.boards)

        self.states = []

        for i in range(len(self.boards)):
            list = [0, 0, 0, 0, 0, 0, 0, 0]
            self.states.append(list)


    def update(self):
        for chip in self.boards:
            chip.update()

        # write updated data to self.states here!!!!!!!!!!!!!!
        for i in range(len(self.boards)):
            for j in range(8):
                self.states[i][j] = self.boards[i].states[j]

        # print(self.states)


def scan_i2c(scl_pin, sda_pin):
    i2c = machine.I2C(scl=machine.Pin(scl_pin), sda=machine.Pin(sda_pin))
    adresses = i2c.scan()                      # scan for slaves, returning a list of 7-bit addresses
    print('found adresses:')
    print(adresses)

# end of file
# end of file. ############################################################################ Setup pins
pin2 = machine.Pin(2,machine.Pin.OUT)

# and write to them
pin2.value(1)
sleep(0.5)
pin2.value(0)
# end of file. ############################################################################ timer
# start_time = ticks_us()
# print("measuring the Time it takes to print this") # check time to print this
# sleep(0.1)
# delay = ticks_diff(ticks_us(), start_time) # calculate delay
# print(delay)
# print('in microseconds!')

# end of file. ############################################################################start of file main_dev.py - this is the main, but renamed to solve namespace problems

machine.freq(160000000)

print("starting Electricon")


## Program loop
scan_i2c(4, 16)

i2c = machine.I2C(scl=machine.Pin(4), sda=machine.Pin(16))
i2c.init(scl=machine.Pin(4), sda=machine.Pin(16))

adress_list = []
adress_list.append(56)
adress_list.append(33)


# buttons = PCF8574_Array(i2c, adress_list)


# temp = machine.ADC(ADC1_PIN)

adc1 = AnalogPin(25)

while True:

    # print("---------------update--------------")
    # time_before =  ticks_us()

    # buttons.update()

    adc1.update()
    sleep(0.02)


    # time_after =  ticks_us()

    # duration = time_after - time_before

    # print("time used for polling chips: ",end=' ')
    # print(duration / 1000)
    #
    # print("board 0, pin 0",end=' ')
    # print(buttons.boards[0].states[0])

    # pcf8574.update()
    # for i in range(8):
    #     print('i: ')
    #     print(pcf8574.states[i])





#end of file
# end of file. ###########################################################################