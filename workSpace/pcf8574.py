
# start of file


class PCF8574:
    def __init__(self, addr, i2c):
        self.pinMode = [0, 0, 0, 0, 0, 0, 0, 0]
        self.states = [0, 0, 0, 0, 0, 0, 0, 0]
        self.buf = 0
        self.last_updated = ticks_ms()
        self.i2c = i2c

        # self.i2c.init()
        self.addr = addr

        # set outputs?
        # self.i2c.write(0b11111111)
        # self.i2c.write(0b00000000)

        # deactivated address-validator
        # adresses = self.i2c.scan()
        # if not addr in adresses:
        #     print('desired adress not available')
        # else:
        #     self.addr = addr

    def update(self):
        self.buf = self.i2c.readfrom(self.addr, 64)
        # self.buf = '23463453'
        self.decode(self.buf)

    def decode(self, data_in):

        self.buf = str(data_in)
        self.buf = self.buf[4:6]
        self.buf = self.hex_to_dec(self.buf)
        self.buf = int(self.buf)
        # self.bata_buffer = '{:08b}'.format(self.data_buffer)
        # self.bata_buffer = format(data_buffer, '#010b') ## convert to bit format
        # print('read data:')
        # print(self.buf)

        if self.buf > 127:
            self.states[7] = 1
            self.buf -= 128
        else:
            self.states[7] = 0

        if self.buf > 63:
            self.states[6] = 1
            self.buf -= 64
        else:
            self.states[6] = 0

        if self.buf > 31:
            self.states[5] = 1
            self.buf -= 32
        else:
            self.states[5] = 0

        if self.buf > 15:
            self.states[4] = 1
            self.buf -= 16
        else:
            self.states[4] = 0

        if self.buf > 7:
            self.states[3] = 1
            self.buf -= 8
        else:
            self.states[3] = 0

        if self.buf > 3:
            self.states[2] = 1
            self.buf -= 4
        else:
            self.states[2] = 0

        if self.buf > 1:
            self.states[1] = 1
            self.buf -= 2
        else:
            self.states[1] = 0

        if self.buf == 1:
            self.states[0] = 1
        else:
            self.states[0] = 0

    def hex_to_dec(self, input_string):

        ans = 0
        string = input_string

        if len(string) == 2:

            if string[1] == '0':
                ans += 0
            elif string[1] == '1':
                ans += 1
            elif string[1] == '2':
                ans += 2
            elif string[1] == '3':
                ans += 3
            elif string[1] == '4':
                ans += 4
            elif string[1] == '5':
                ans += 5
            elif string[1] == '6':
                ans += 6
            elif string[1] == '7':
                ans += 7
            elif string[1] == '8':
                ans += 8
            elif string[1] == '9':
                ans += 9
            elif string[1] == 'a':
                ans += 10
            elif string[1] == 'b':
                ans += 11
            elif string[1] == 'c':
                ans += 12
            elif string[1] == 'd':
                ans += 13
            elif string[1] == 'e':
                ans += 14
            elif string[1] == 'f':
                ans += 15

            if string[0] == '0':
                ans += 16 * 0
            elif string[0] == '1':
                ans += 16 * 1
            elif string[0] == '2':
                ans += 16 * 2
            elif string[0] == '3':
                ans += 16 * 3
            elif string[0] == '4':
                ans += 16 * 4
            elif string[0] == '5':
                ans += 16 * 5
            elif string[0] == '6':
                ans += 16 * 6
            elif string[0] == '7':
                ans += 16 * 7
            elif string[0] == '8':
                ans += 16 * 8
            elif string[0] == '9':
                ans += 16 * 9
            elif string[0] == 'a':
                ans += 16 * 10
            elif string[0] == 'b':
                ans += 16 * 11
            elif string[0] == 'c':
                ans += 16 * 12
            elif string[0] == 'd':
                ans += 16 * 13
            elif string[0] == 'e':
                ans += 16 * 14
            elif string[0] == 'f':
                ans += 16 * 15
            # print(reading)
            return ans
        else:
            print('invalid string!')


class PCF8574_Array:
    def __init__(self, i2c, addr_list):
        self.i2c = i2c

        self.boards = []

        for addr in addr_list:
            chip = PCF8574(addr, self.i2c)
            self.boards.append(chip)

        print("initialisation complete. found and connected to",end=' ')
        print(len(self.boards),end=' ')
        print("boards.")

        self.num_boards = len(self.boards)

        self.states = []

        for i in range(len(self.boards)):
            list = [0, 0, 0, 0, 0, 0, 0, 0]
            self.states.append(list)


    def update(self):
        for chip in self.boards:
            chip.update()

        # write updated data to self.states here!!!!!!!!!!!!!!
        for i in range(len(self.boards)):
            for j in range(8):
                self.states[i][j] = self.boards[i].states[j]

        # print(self.states)


def scan_i2c(scl_pin, sda_pin):
    i2c = machine.I2C(scl=machine.Pin(scl_pin), sda=machine.Pin(sda_pin))
    adresses = i2c.scan()                      # scan for slaves, returning a list of 7-bit addresses
    print('found adresses:')
    print(adresses)

# end of file
